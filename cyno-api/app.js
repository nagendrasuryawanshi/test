const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;


//my import
const providerRouter = require('./api/routes/providers');
const userRouter = require('./api/routes/users');
const companyRouter = require('./api/routes/companies');
const serviceRouter = require('./api/routes/services');
const bookingRouter = require('./api/routes/bookings');
const paymentRouter = require('./api/routes/payment');
//mongoose connection
//mongoose.connect('mongodb://localhost:27017/cynodb');

var db = "mongodb://localhost:27017/cynodb";

  mongoose.connect(db, function(err, response){
    if(err){
      console.log(' Failed to connected to ' + db);
    }
    else{
      console.log(' Connected to ' + db);
    }
  });

//Middlewares//
app.use(bodyParser.json());

app.use(bodyParser.urlencoded({
    extended: true
}))

app.use(morgan('dev'));

app.use('/uploads',()=>{
    console.log(path.join(), '+++++++++');
});

app.use((request, response, next)=>{
    response.header('Access-Control-Allow-Origin', '*');
    response.header('Access-Control-Allow-Headers', '*');

    if(request.method === 'OPTIONS'){
        response.header('Access-Control-Allow-Method', 'PUT, PATCH, DELETE, GET, POST');
        return response.status(200).json({});
    }
    next();
});

app.use('/user', userRouter)
app.use('/providers', providerRouter);
app.use('/company', companyRouter);
app.use('/service', serviceRouter);
app.use('/booking', bookingRouter);
app.use('/payment', paymentRouter);
module.exports = {
    app
}