const express = require('express');
const router = express.Router();
const userController = require('../controllers/usercontroller');
const authMiddleware = require('../middleware/auth');

//router.get('/', authMiddleware, userController.getUser);
router.get('/',  userController.getUser);

router.post('/signup', userController.sign_up)

router.post('/login', userController.login);

module.exports = router;