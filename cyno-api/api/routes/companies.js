const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const { Company } = require('../models/company');


router.get('/',(req,res,next)=>{
    Company.find()
    .exec()
    .then(docs=>{
        console.log(docs);
        res.status(200).json(docs);
    })
    .catch(err=>{
        console.log(err);
        res.status(500).json({
            error:err
        });
    });
  
})
router.post('/', (request, response)=>{
    console.log(request.body);
    var body = request.body;
    var company = new Company({
        _id: new mongoose.Types.ObjectId(),
        companyName: body.companyName,
        contactFirstName: body.contactFirstName,
        contactLastName: body.contactLastName,
        contactEmail: body.contactEmail,
        contactPhone: body.contactPhone,
        streetNumber: body.streetNumber,
        streetName: body.streetName,
        apartmentNumber: body.apartmentNumber,
        city: body.city,
        province: body.province,
        postalCode: body.postalCode,
        country: body.country
    });
    company.save()
    .then((result)=>{
        console.log(result);
        response.status(200).json(result);
    })
    .catch((error)=>{
        console.log(error);
        response.status(400).json({error:error});
    })
})

router.get('/:companyId',(req,res,next)=>{
    const id=req.params.companyId;
   console.log(id);
   Company.findById(id)
   .exec()
   .then(doc=>{
       console.log(doc);
       if(doc){
       res.status(200).json(doc);
       }else{
           res.status(404).json({
               message:"No valid entry found for provided ID"
           });
       }

   })
   .catch(err=>{
       console.log(err);
       res.status(500).json({error:err});
       });
    
})
router.patch('/:companyId',(req,res,next)=>{    
    const id=req.params.companyId;
   const updateOps={};
   for(const ops of req.body){
       updateOps[ops.propName]=ops.value;
   }
   console.log( id);
   Company.update({_id:id},{ $set:updateOps})
   .exec()
   .then(result=>{
       console.log(result);
       res.status(200).json(result);
   })
   .catch(err=>{
       console.log(err);
       res.status(500).json({
           error:err
       });
   });
       res.status(200).json({
       message:'Update product'
       });     
})

router.delete('/:companyId',(req,res,next)=>{    
    const id=req.params.companyId;
   Company.remove({_id:id})
   .exec()
   .then(result=>{
       res.status(200).json(result);
   })
   .catch(err=>{
       console.log(err);
       res.status(500).json({
           error:err
       });
   })
   // res.status(200).json({
   // message:'Product deleted',
   // _id:id
   // });     
})

module.exports = router;