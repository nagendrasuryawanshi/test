const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');

const { Booking } = require('../models/booking');


router.get('/',(req,res,next)=>{
    Booking.find()
    .exec()
    .then(docs=>{
        console.log(docs);
        res.status(200).json(docs);
    })
    .catch(err=>{
        console.log(err);
         res.status(500).json({
                      error:err
        });
    });
  
})

router.post('/', (request, response)=>{
    console.log(request.body);
    var body = request.body;
    var booking = new Booking({
        _id: new mongoose.Types.ObjectId(),
        activePeriodKey: body.activePeriodKey,
        additionalData:  body.additionalData,
        apiKey:  body.apiKey,
        bookingStatus:  body.bookingStatus,
        clientData: body.clientData,
        clientSbmId:  body.clientSbmId,
        clientUid:  body.clientUid,
        companyStartDateTime:  body.companyStartDateTime,
        companyTimezone:  body.companyTimezone,
        confirmationData: body.confirmationData,
        key:  body.key,
        providerSbmId:  body.providerSbmId,
        providerUid:  body.providerUid,
        serviceKey:  body.serviceKey,
        serviceSbmId:  body.serviceSbmId,
        sessionId:  body.sessionId,
        startDate:  body.startDate,
        startTime:  body.startTime,
        token:  body.token,    
        userTimezone:  body.userTimezone
    });
    booking.save()
    .then((result)=>{
      //  console.log(result);
        response.status(200).json(result);
    })
    .catch((error)=>{     
        response.status(400).json({error:error});
    })
})

router.get('/:bookingId',(req,res,next)=>{
    const id=req.params.bookingId;
   console.log(id);
   Booking.findById(id)
   .exec()
   .then(doc=>{
       console.log(doc);
       if(doc){
       res.status(200).json(doc);
       }else{
           res.status(404).json({
               message:"No valid entry found for provided ID"
           });
       }
   })
   .catch(err=>{
       console.log(err);
       res.status(500).json({error:err});
       });    
})

router.patch('/:bookingId',(req,res,next)=>{    
    const id=req.params.bookingId;
   const updateOps={};
   //console.log(req.body);
   for(const ops of req.body){
    console.log("---");
       console.log(ops);
       updateOps[ops.propName]=ops.value;
   }
   //console.log( id);
   Booking.update({_id:id},{ $set:updateOps})
   .exec()
   .then(result=>{
       //console.log(result);
       res.status(200).json(result);
   })
   .catch(err=>{
      // console.log(err);
       res.status(500).json({err:err});
   });
   
})               

router.delete('/:bookingId',(req,res,next)=>{    
    const id=req.params.bookingId;
   Booking.remove({_id:id})
   .exec()
   .then(result=>{
       res.status(200).json(result);
   })
   .catch(err=>{
       console.log(err);
       res.status(500).json({ error:err    });
   })
   
})

module.exports = router;
