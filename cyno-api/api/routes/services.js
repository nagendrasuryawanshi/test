const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();
const { Service } = require('../models/service');

router.get('/', (req, res)=>{
    Service.find()
    .exec()
    .then(docs=>{
        console.log(docs);
        res.status(200).json(docs);
    })
    .catch(err=>{
        console.log(err);
        res.status(500).json({
            error:err
        });
    });
});

router.post('/', (request, response)=>{
    var service = new Service({
        _id: new mongoose.Types.ObjectId(),
        SBMid: request.body.SBMid,
        audience: request.body.audience,
        company: request.body.company,
        designation: request.body.designation,
        coverPhotoURL: request.body.coverPhotoURL,
        description: request.body.description,
        duration: request.body.duration,
        fullPrice: request.body.fullPrice,
        type: request.body.type     
    });

    service
    .save()
    .then((result)=>{
        console.log(result);
        response.status(201).json(result)
    })
    .catch((err)=>{
        response.status(400).json({
            message: err
        })
    })
})

router.get('/:serviceId',(req,res,next)=>{
    const id=req.params.serviceId;
   console.log(id);
   Service.findById(id)
   .exec()
   .then(doc=>{
       console.log(doc);
       if(doc){
       res.status(200).json(doc);
       }else{
           res.status(404).json({
               message:"No valid entry found for provided ID"
           });
       }
   })
   .catch(err=>{
       console.log(err);
       res.status(500).json({error:err});
       });
    
})
router.patch('/:serviceId',(req,res,next)=>{    
    const id=req.params.serviceId;
   const updateOps={};
   for(const ops of req.body){
       updateOps[ops.propName]=ops.value;
   }
   console.log( id);
   Service.update({_id:id},{ $set:updateOps})
   .exec()
   .then(result=>{
       console.log(result);
       res.status(200).json(result);
   })
   .catch(err=>{
       console.log(err);
       res.status(500).json({
           error:err
       });
   });
       res.status(200).json({
       message:'Update provider'
       });     
})

router.delete('/:serviceId',(req,res,next)=>{    
    const id=req.params.serviceId;
    Service.remove({_id:id})
   .exec()
   .then(result=>{
       res.status(200).json(result);
   })
   .catch(err=>{
       console.log(err);
       res.status(500).json({
           error:err
       });
   })
   // res.status(200).json({
   // message:'Product deleted',
   // _id:id
   // });     
})
module.exports = router;