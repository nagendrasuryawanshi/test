const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const { Provider } = require('../models/provider');
const multer = require('multer');
const storage = multer.diskStorage({
    destination: function(req, file, callback){
        callback(null, 'uploads')
    },
    filename: function(req, file, callback){
        var localDate = new Date().getTime();
        callback(null, `${localDate}-${file.originalname}`)
    }
});

const fileFilter = (req, file, cb)=>{
    if(file.mimetype == 'image/png' || file.mimetype == 'image/jpeg'){
        cb(null, true)
    }else{
        cb(null, false)
    }
} 

const upload = multer({
    storage: storage,
    fileFilter: fileFilter
});

router.get('/', (req, res)=>{
    Provider.find()
    .exec()
    .then(docs=>{
        console.log(docs);
        res.status(200).json(docs);
    })
    .catch(err=>{
        console.log(err);
        res.status(500).json({
            error:err
        });
    });
});

router.post('/', upload.single('avatar'), (request, response)=>{
    var provider = new Provider({
        _id: new mongoose.Types.ObjectId(),
        firstName: request.body.firstName,
        lastName: request.body.lastName,
        biography: request.body.biography,
        designation: request.body.designation,
        email: request.body.email,
        experties: request.body.experties,
        isTaxExempt: request.body.isTaxExempt,
        licenseNumber: request.body.licenseNumber,
        providerCompany: request.body.providerCompany,
        taxNumber: request.body.taxNumber,
        vimeoId: request.body.vimeoId,
        vimeoThumbnailURL: request.body.vimeoThumbnailURL,
    });

    provider
    .save()
    .then((result)=>{
        console.log(result);
        response.status(201).json(result)
    })
    .catch((err)=>{
        response.status(400).json({
            message: err
        })
    })
})

router.get('/:providerId',(req,res,next)=>{
    const id=req.params.providerId;
   console.log(id);
   Provider.findById(id)
   .exec()
   .then(doc=>{
       console.log(doc);
       if(doc){
       res.status(200).json(doc);
       }else{
           res.status(404).json({
               message:"No valid entry found for provided ID"
           });
       }
   })
   .catch(err=>{
       console.log(err);
       res.status(500).json({error:err});
       });
    
})
router.patch('/:providerId',(req,res,next)=>{    
    const id=req.params.providerId;
   const updateOps={};
   for(const ops of req.body){
       updateOps[ops.propName]=ops.value;
   }
   console.log( id);
   Provider.update({_id:id},{ $set:updateOps})
   .exec()
   .then(result=>{
       console.log(result);
       res.status(200).json(result);
   })
   .catch(err=>{
       console.log(err);
       res.status(500).json({
           error:err
       });
   });
       res.status(200).json({
       message:'Update provider'
       });     
})

router.delete('/:providerId',(req,res,next)=>{    
    const id=req.params.providerId;
   Provider.remove({_id:id})
   .exec()
   .then(result=>{
       res.status(200).json(result);
   })
   .catch(err=>{
       console.log(err);
       res.status(500).json({
           error:err
       });
   })
   // res.status(200).json({
   // message:'Product deleted',
   // _id:id
   // });     
})
module.exports = router;