const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const { Payment } = require('../models/payment');


router.get('/',(req,res,next)=>{
    Payment.find()
    .exec()
    .then(docs=>{
        console.log(docs);
        res.status(200).json(docs);
    })
    .catch(err=>{
        console.log(err);
        res.status(500).json({
            error:err
        });
    });
  
})
router.post('/', (request, response)=>{
    console.log(request.body);
    var body = request.body;
    var payment = new Payment({
        _id: new mongoose.Types.ObjectId(),      
        charge:body.charge ,
        clientId:body.clientId,
        companyId:body.companyId,
        isComplete:body.isComplete,
        method:body.method,
        paymentDate:body.paymentDate,
        percentTax:body.percentTax,
        providerId:body.providerId,
        purchasedActivePeriod:body.purchasedActivePeriod,
        serviceId:body.serviceId,
        servicePeriod:body.servicePeriod,
        token:body.token
    });
    payment.save()
    .then((result)=>{
        console.log(result);
        response.status(200).json(result);
    })
    .catch((error)=>{
        console.log(error);
        response.status(400).json({error:error});
    })
})

router.get('/:paymentId',(req,res,next)=>{
    const id=req.params.paymentId;
   console.log(id);
   Payment.findById(id)
   .exec()
   .then(doc=>{
       console.log(doc);
       if(doc){
       res.status(200).json(doc);
       }else{
           res.status(404).json({
               message:"No valid entry found for provided ID"
           });
       }

   })
   .catch(err=>{
       console.log(err);
       res.status(500).json({error:err});
       });
    
})
router.patch('/:paymentId',(req,res,next)=>{    
    const id=req.params.paymentId;
   const updateOps={};
   for(const ops of req.body){
       updateOps[ops.propName]=ops.value;
   }
   console.log( id);
   Payment.update({_id:id},{ $set:updateOps})
   .exec()
   .then(result=>{
       console.log(result);
       res.status(200).json(result);
   })
   .catch(err=>{
       console.log(err);
       res.status(500).json({
           error:err
       });
   });
       res.status(200).json({
       message:'Payment Update'
       });     
})

router.delete('/:paymentId',(req,res,next)=>{    
    const id=req.params.paymentId;
    Payment.remove({_id:id})
   .exec()
   .then(result=>{
       res.status(200).json(result);
   })
   .catch(err=>{
       console.log(err);
       res.status(500).json({
           error:err
       });
   })
  
})

module.exports = router;