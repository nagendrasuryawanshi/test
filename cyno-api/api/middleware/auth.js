const jwt = require('jsonwebtoken');
const { User } = require('../models/user')
const errorMsg = require('../controllers/error');
module.exports = (request, response, next)=>{
    const token = request.headers.token;
    try{
        jwt.verify(token, "cyno_pass_key");
        next();
    }catch(e){
        errorMsg.errorMessage(response, 404, 'Invalid token')
    }
    
}