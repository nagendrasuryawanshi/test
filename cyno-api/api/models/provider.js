const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var providerSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    firstName:{
        type: String,
        required: true,
        maxlength: 50
    },
    lastName: {
        type: String,
        required: true,
        maxlength: 50
    },
    biography: {
        type: String,
        required: true
    },
    designation: {
        type: String,
        required: true
    },
    email:{
        type: String,
        required: true
    },
    experties: {
        type: String,
        required: true
    },
    isTaxExempt:{
        type: Boolean
    },
    licenseNumber:{
        type: String
    },
    providerCompany:{
        type: mongoose.Schema.Types.ObjectId
    },
    taxNumber:{
        type: String
    },
    vimeoId:{
        type: Number
    },
    vimeoThumbnailURL:{
        type: String
    }
});

var Provider = mongoose.model('Provider', providerSchema);

module.exports = {
    Provider
}