const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const companySchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    companyName: {
        type: String,
        required: true
    },
    contactFirstName:{
        type: String,
        required: true,
        maxlength: 50
    },
    contactLastName:{
        type: String,
        required: true,
        maxlength: 50
    },
    contactEmail:{
        type: String,
        required: true,
        match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    },
    contactPhone:{
        type: Number,
        required: true
    },
    streetNumber:{
        type: Number,
        required: true
    },
    streetName:{
        type: String,
        required: true,
        maxlength: 50
    },
    apartmentNumber:{
        type: Number
    },
    city:{
        type: String,
        required: true
    },
    province:{
        type: String,
        required: true
    },
    postalCode:{
        type: Number,
        required: true
    },
    country:{
        type: String,
        required: true
    }
});

const Company = mongoose.model('Company', companySchema);

module.exports = {
    Company
}