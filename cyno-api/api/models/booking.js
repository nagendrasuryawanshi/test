const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const bookingSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,

    activePeriodKey: { type: String},

    additionalData: { type: Object},
    
    apiKey: { type: String},
    bookingStatus: { type: String},

    clientData:{ type: Object},

    clientSbmId: { type: Number},
    clientUid: { type: String},
    companyStartDateTime: { type: Date},
    companyTimezone: { type: String},

    confirmationData:{ type: Object},

    key: { type: String},
    providerSbmId: { type: Number},
    providerUid: { type: String},
    serviceKey: { type: String},
    serviceSbmId: { type: Number},
    sessionId: { type: String},
    startDate: { type: String},
    startTime: { type: String},
    token: { type: String},    
    userTimezone: { type: String}
});

const Booking = mongoose.model('Booking', bookingSchema);

module.exports = {Booking}