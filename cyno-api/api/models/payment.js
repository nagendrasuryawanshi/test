const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const paymentSchema = new Schema({    
    _id: mongoose.Schema.Types.ObjectId,
    charge: {
        type: Object,
        required: true
    },
    clientId:{
        type: String,
        required: true      
    },
    companyId:{
        type: String,
        required: true
    },
    isComplete:{
        type: Boolean,
        required: true      
    },
    method:{
        type: Object,
        required: true
    },
    paymentDate:{
        type: String,
        required: true
    },
    percentTax:{
        type: Number,
        required: true       
    },
    providerId:{
        type: String
    },
    purchasedActivePeriod:{
        type: String
    },
    serviceId:{
        type: String
    },
    servicePeriod:{
        type: Object
    },
    token:{
        type: Object
    }
});

const Payment = mongoose.model('Payment', paymentSchema);

module.exports = {
        Payment
}