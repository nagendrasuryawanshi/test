const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var serviceSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    SBMid: {
        type: Number
        //,required: true
    },
    audience:{
        type: String
    },
    company: {
        type: String
    },
    coverPhotoURL:{
        type: String
    },
    description: {
        type: String
    },
    duration:{
        type: String
    },
    fullPrice:{
        type: String
    },
    type:{
        type: String
    }
});

var Service = mongoose.model('Service', serviceSchema);

module.exports = {
    Service
}