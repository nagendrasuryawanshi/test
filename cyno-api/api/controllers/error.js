exports.errorMessage = function(response, statusCode ,message){
    return response.status(statusCode).json({
        status: 0,
        error: message
    })
}