const { User } = require('../models/user');
const _ = require('lodash');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const errorMsg = require('./error');

exports.getUser = (request, response)=>{
    User.find({})
    //.select('email')
    .exec()
    .then((result)=>{
        response.status(200).json({
            status: 11,
            result
        })
    })
    .catch((err)=>{
        return errorMsg.errorMessage(response, 400, "Not found")
    })
}

exports.sign_up =  (request, response)=>{
    var body = request.body;
    if(body.password === body.confirmPassword){
        User.find({
            email: body.email
        })
        .exec()
        .then((result)=>{
            if(result.length > 0){
                return errorMsg.errorMessage(response, 400, "Email already exists")
            }
            bcrypt.hash(body.password, 10, (err, hash)=>{
                if(err){
                    return errorMsg.errorMessage(response, 400, "Enter password")
                }
    
                var user = new User({
                    email: body.email,
                    password: hash
                });
    
                return user.save().then((res)=>{            
                    response.status(200).json({
                        status: 1,
                        message: "Successfully register"
                    });
                })
            });    
        })
        .catch((err)=>{
            return errorMsg.errorMessage(response, 400, "Bad request");
        })
    }else{
        return errorMsg.errorMessage(response, 403, "Password do not match");
    } 
}


exports.login = (request, response)=>{
    var body = request.body;
    User.findOne({
        email: body.email
    })
    .exec()
    .then((result)=>{
        if(!result){
            return errorMsg.errorMessage(response, 400, "No user found");
        }
        bcrypt.compare(body.password, result.password, (err, res)=>{
            if(err){
                return errorMsg.errorMessage(response, 400,"Something went wrong");
            }
            if(res){
                jwt.sign({
                    email: result.email,
                    _id: result._id,
                    password: body.password 
                }, "cyno_pass_key", (e, token)=>{
                    if(e){
                        return errorMsg.errorMessage(response, 400 ,"Something went wrong");
                    }  
                    response.status(200).json({
                        status: 1,
                        message: "Successfully logged in",
                        userId: result._id,
                        token
                    })                  
                })
            }else{
                return errorMsg.errorMessage(response, 400, "Incorrect Password")
            }
        })
    })
    .catch((err)=>{
        return errorMsg.errorMessage(response, 404, "Not Found");
    })
}
